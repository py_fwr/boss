# -*- coding: utf-8 -*-

# Scrapy settings for boss project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'boss'

SPIDER_MODULES = ['boss.spiders']
NEWSPIDER_MODULE = 'boss.spiders'

CITYID = '101210100'  # 杭州

LANGUAGE = 'Python'  # Python

# IP代理池
IPPOOL = [
    "http://119.162.150.192:8118",
    "http://112.255.118.2:8118",
    "http://221.225.147.61:8118",
    "http://182.88.130.201:8118",
    "http://122.137.4.230:8118"
]

# cookies池
COOKIES_POOL = [
{'_uab_collina': '154657433092573031894575', ' lastCity': '101210100', ' t': 'GGblmNKhfhtEOcWh', ' wt': 'GGblmNKhfhtEOcWh', ' JSESSIONID': '09F2119B1D2E3E72584D3DE9CF092D95', ' __c': '1557933858', ' __g': '-', ' __l': 'l=%2Fwww.zhipin.com%2F&r=', ' Hm_lvt_194df3105ad7148dcf2b98a91b5e727a': '1557835228,1557887152,1557931870,1557933861', ' __a': '74214151.1546574330.1557931865.1557933858.1878.127.4.569', ' Hm_lpvt_194df3105ad7148dcf2b98a91b5e727a': '1557933913'},
{'_uab_collina': '154657433092573031894575', ' lastCity': '101210100', ' JSESSIONID': '09F2119B1D2E3E72584D3DE9CF092D95', ' __c': '1557933858', ' __g': '-', ' __l': 'l=%2Fwww.zhipin.com%2F&r=', ' Hm_lvt_194df3105ad7148dcf2b98a91b5e727a': '1557835228,1557887152,1557931870,1557933861', ' t': 'DPMDO9qvnhUxVSOh', ' wt': 'DPMDO9qvnhUxVSOh', ' __a': '74214151.1546574330.1557931865.1557933858.1884.127.10.575', ' Hm_lpvt_194df3105ad7148dcf2b98a91b5e727a': '1557933998'}
]

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3534.4 Safari/537.36'

LOG_LEVEL = 'WARNING'
# Obey robots.txt rules
ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 0.25
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
COOKIES_ENABLED = True

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
#     'Accept-Language': 'zh-CN,zh;q=0.9',
#     'cookie': '_uab_collina=154657433092573031894575; lastCity=101210100; __c=1557887151; __g=-; __l=l=%2Fwww.zhipin.com%2F&r=; Hm_lvt_194df3105ad7148dcf2b98a91b5e727a=1557641898,1557709206,1557835228,1557887152; JSESSIONID=35913225368B2984DD282184F185829E; t=DPMDO9qvnhUxVSOh; wt=DPMDO9qvnhUxVSOh; __a=74214151.1546574330.1557835224.1557887151.1863.125.117.554; Hm_lpvt_194df3105ad7148dcf2b98a91b5e727a=1557931128'
# }

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'boss.middlewares.BossSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
   'boss.middlewares.ProxyMiddleware': 543,
}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   'boss.pipelines.BossPipeline': 300,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
