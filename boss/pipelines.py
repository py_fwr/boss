# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import re

from openpyxl import Workbook


class BossPipeline(object):
    def __init__(self):
        self.wb = Workbook()
        self.ws = self.wb.active
        # 设置表头
        self.ws.append(['城市', '工作名称', '薪资', '经验要求', '学历要求', '公司名字', '公司地址', '工作描述', '地址链接', '公司官网', '发布日期'])

    def process_item(self, item, spider):
        item["text"] = self.process_content(item["text"])
        line = [
            item['job_city'],
            item['name'],
            item['salary'],
            item['experience'],
            item['edu'],
            item['company'],
            item['address'],
            item['text'],
            item['url'],
            item['company_url'],
            item['data']
        ]

        self.ws.append(line)
        self.wb.save('boss515.xlsx')

        return item

    def process_content(self, content):  # 处理content字段的数据
        content = [re.sub("\xa0|\s", "", i) for i in content]  # 替换字符串中的\xa0,\s
        content = [i for i in content if len(i) > 0]  # 删除列表中的空字符串

        res_str = ''
        for s in content:
            res_str = res_str + s + '\r\n'

        return res_str
