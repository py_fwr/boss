# -*- coding: utf-8 -*-
from copy import deepcopy

import scrapy
from scrapy.conf import settings


class ZhipinSpider(scrapy.Spider):
    name = 'zhipin'
    allowed_domains = ['www.zhipin.com']
    start_urls = ['https://www.zhipin.com/c{}/?query={}&page=1'.format(settings.get('CITYID'), settings.get('LANGUAGE'))]

    def parse(self, response):
        print(response.url)

        try:
            a = response.url.split('?')
            temp = a[1]
            temp2 = temp.split('&')
            res = {i.split('=')[0]: i.split('=')[1] for i in temp2}
            print('当前正在爬取第{}页数据'.format(res['page']))
        except Exception as e:
            pass

        li_list = response.xpath("//div[@class='job-list']/ul/li")

        for li in li_list:
            item = {}
            item['name'] = li.xpath(".//div[@class='job-title']/text()").extract_first() # 工作名称
            item['salary'] = li.xpath(".//div[@class='info-primary']/h3/a/span/text()").extract_first() # 工作薪资
            item['url'] = li.xpath(".//div[@class='info-primary']/h3/a/@href").extract_first() # url
            item['url'] = 'https://www.zhipin.com' + item['url']

            yield scrapy.Request(
                item['url'],
                callback=self.parse_detail,
                meta={"item": deepcopy(item)}
            )

        next_url = response.xpath("//a[@class='next']/@href").extract_first()
        if next_url:

            next_url = 'https://www.zhipin.com' + next_url
            yield scrapy.Request(next_url, callback=self.parse)

    def parse_detail(self, response):

        item = response.meta["item"]
        item['text'] = response.xpath("//h3[text()='职位描述']/../div[1]/text()").extract() # 职位描述
        item['job_city'] = response.xpath("//div[@class='job-primary detail-box']/div[@class='info-primary']/p/text()[1]").extract_first() # 城市
        item['experience'] = response.xpath("//div[@class='job-primary detail-box']/div[@class='info-primary']/p/text()[2]").extract_first() # 经验要求
        item['edu'] = response.xpath("//div[@class='job-primary detail-box']/div[@class='info-primary']/p/text()[3]").extract_first() # 学历要求
        item['address'] = response.xpath("//div[@class='job-location']/div[@class='location-address']/text()").extract_first() # 公司地址
        item['company'] = response.xpath("//h3[text()='工商信息']/../div[1]/text()").extract_first() # 公司名字
        item['data'] = response.xpath("//p[text()='公司基本信息']/../p[last()]/text()").extract_first() # 发布日期
        item['company_url'] = response.xpath("//div[@class='sider-company']/p[5]/text()").extract_first() # 公司官网

        yield item